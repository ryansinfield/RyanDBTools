

using System.Collections.Generic;
using System.Data.SqlClient;

namespace RyanDBTools
{
    public class WhereClauseBuilder
    {
        private List<Dictionary<int, SqlParameter[]>> conditions;

        public const int SQL_AND = 1;
        public const int SQL_OR = 2;
        public const int SQL_LIKE = 3;
        public const int SQL_GT = 4;
        public const int SQL_GTE = 5;
        public const int SQL_LT = 6;
        public const int SQL_LTE = 7;
        public const int SQL_NE = 8;
        public const int SQL_EQ = 9;

        public WhereClauseBuilder(Dictionary<int, SqlParameter[]> firstCondition, params Dictionary<int, SqlParameter[]>[] conditions)
        {
            
        }

        public WhereClauseBuilder(List<Dictionary<int, SqlParameter[]>> conditions)
        {
            this.conditions = conditions;
        }

        public void AddCondition(Dictionary<int, SqlParameter[]> condition)
        {
            // Add a new condition to the existing conditions list
            // Validate the dictionary
        }

        public string ToString()
        {

        }
}