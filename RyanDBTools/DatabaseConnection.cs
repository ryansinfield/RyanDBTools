﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RyanDBTools
{
    public class DatabaseConnection : IDisposable
    {
        private SqlConnection con;

        public DatabaseConnection(string connectionString)
        {
            con = new SqlConnection(connectionString);
        }

        public DatabaseConnection (SqlConnection sqlCon)
        {
            this.con = sqlCon;
        }

        /// <summary>
        /// Sets the target database
        /// </summary>
        /// <param name="targetdb">The name of the new target database</param>
        public void SetDatabase(string targetdb)
        {
            con.ChangeDatabase(targetdb);
        }

        /// <summary>
        /// Gets the current target database name
        /// </summary>
        /// <returns>The name of the current target database</returns>
        public string GetDatabase()
        {
            return con.Database;
        }

        /// <summary>
        /// Gets the server name of the current database connection
        /// </summary>
        /// <returns>The name of the host server</returns>
        public string GetServer()
        {
            return con.DataSource;
        }

        /// <summary>
        /// Gets the connection string being used for the database connection
        /// </summary>
        /// <returns>The connection string of the database connection</returns>
        public string GetConnectionString()
        {
            return con.ConnectionString;
        }

        /// <summary>
        /// Opens the database connection
        /// </summary>
        public void Open ()
        {
            con.Open();
        }
        
        /// <summary>
        /// Closes the database connection
        /// </summary>
        public void Close()
        {
            con.Close();
        }

        /// <summary>
        /// Selects specified data from a table or view
        /// </summary>
        /// <param name="tablename">The name of table of view to be queried</param>
        /// <param name="columnsToSelect">A string array containing the column names to return</param>
        /// <param name="stmtParams">The SqlParameter array containing the parameter values</param>
        /// <returns>A SqlDataReader containing the result set of the query</returns>
        public SqlDataReader Select(string tablename, string[] columnsToSelect, SqlParameter[] stmtParams)
        {
            // Construct the SQL query
            string selectClause = "SELECT ";

            // Create SQL statement
            SqlCommand stmt = new SqlCommand();

            // Get a string for the column names in the select statement from the columns array passed
            for (int i = 0; i < columnsToSelect.Length; i++)
            {
                // Add a comma to the selectClause if it is not the first column
                if (i > 0)
                {
                    selectClause += ",";
                }

                // Add the column name to the selectClause
                selectClause += columnsToSelect[i];
            }

            string fromClause = " FROM " + tablename;

            // Get the names of the arguments (must match column names)
            string whereClause = " WHERE ";

            for (int i = 0; i < stmtParams.Length; i++)
            {
                // Add 'AND' if it is not the first parameter
                if (i > 0)
                {
                    whereClause += " AND ";
                }

                // Trims the @ from the parameter name to add to the clause = parameter name
                whereClause += stmtParams[i].ParameterName.Substring(1) + "=" + stmtParams[i].ParameterName;

                // Also add parameter to the stmt
                stmt.Parameters.Add(stmtParams[i]);
            }

            // Set the stmt command text
            string cmd = selectClause + fromClause + whereClause;
            stmt.CommandText = cmd;
            stmt.Connection = this.con;

            // Execute statement and return the results
            SqlDataReader results = stmt.ExecuteReader();
            stmt.Parameters.Clear();
            return results;
        }

        /// <summary>
        /// Selects all records from a given table or view
        /// </summary>
        /// <param name="tablename">The name of the table or view to query</param>
        /// <returns>A SqlDataReader containing the result set of the query</returns>
        public SqlDataReader SelectAll(string tablename)
        {
            // Select all records from a table
            SqlCommand stmt = new SqlCommand()
            {
                CommandText = "SELECT * FROM " + tablename,
                Connection = this.con
            };

            // Execute statement and reutnr the results
            SqlDataReader results = stmt.ExecuteReader();
            stmt.Parameters.Clear();
            return results;
        }

        /// <summary>
        /// Execute a stored procedure with input parameters
        /// </summary>
        /// <param name="procedureName">The name of the stored procedure</param>
        /// <param name="inParams">An array of SqlParameters that match input parameters for the stored procedure</param>
        /// <returns>The number of rows affected by the stored procedure</returns>
        public int ExecuteStoredProcedure(string procedureName, SqlParameter[] inParams=null)
        {
            // Execute a SqlCommand
            SqlCommand cmd = new SqlCommand(procedureName, con)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Bind params
            if (inParams != null)
            {
                foreach (SqlParameter param in inParams)
                {
                    cmd.Parameters.Add(param);
                }
            }

            // Execute
            int rowsAffected = cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            return rowsAffected;
        }

        /// <summary>
        /// Insert record(s) into a given table
        /// </summary>
        /// <param name="tablename">The name of the table to insert into</param>
        /// <param name="inParams">The parameters representing the record(s) to insert</param>
        /// <returns>The number of rows affected</returns>
        public int Insert(string tablename, SqlParameter[] inParams)
        {
            // Insert a record into the database
            // Build the insert statement
            SqlCommand stmt = new SqlCommand();

            // Generate parameter list
            string parms = "(";
            for (int i = 0; i < inParams.Length; i++)
            {
                if (i > 0) parms += ", ";           // add commas before all but first record
                parms += inParams[i].ParameterName; // append the parameter statement to the command text
                stmt.Parameters.Add(inParams[i]);   // add parameter to the actual statement
            }
            parms += ")";

            string cmdText = @"INSERT INTO " + tablename + " VALUES " + parms + ";";
            stmt.Connection = this.con;
            stmt.CommandText = cmdText;
            int rowsAffected = stmt.ExecuteNonQuery();
            stmt.Parameters.Clear();
            return rowsAffected;
        }

        /// <summary>
        /// Insert record(s) into a given table
        /// </summary>
        /// <param name="tablename">The name of the table to insert into</param>
        /// <param name="columnsToInsert">The name of the columns to insert into</param>
        /// <param name="inParams">The parameters representing the record(s) to insert</param>
        /// <returns>The number of rows affected</returns>
        public int Insert(string tablename, string[] columnsToInsert, SqlParameter[] inParams)
        {
            if (columnsToInsert.Length != inParams.Length)
                throw new DatabaseConnectionException("Columns and parameters arrays are of different sizes.");

            // Insert a record into the database
            // Build the insert statement
            SqlCommand stmt = new SqlCommand();

            string insCols = "(";
            for (int i = 0; i < columnsToInsert.Length; i++)
            {
                if (i > 0) insCols += ", ";     // add a comma if not the first record
                insCols += columnsToInsert[i];  // append the column name to the insert column list
            }
            insCols += ")";

            // Generate parameter list
            string parms = "(";
            for (int i = 0; i < inParams.Length; i++)
            {
                if (i > 0) parms += ", ";           // add commas before all but first record
                parms += inParams[i].ParameterName; // append the parameter statement to the command text
                stmt.Parameters.Add(inParams[i]);   // add parameter to the actual statement
            }
            parms += ")";

            string cmdText = @"INSERT INTO " + tablename + " " + insCols + " VALUES " + parms + ";";
            stmt.Connection = this.con;
            stmt.CommandText = cmdText;
            int rowsAffected = stmt.ExecuteNonQuery();
            stmt.Parameters.Clear();
            return rowsAffected;
        }

        /// <summary>
        /// Execute a raw query string (use with caution to prevent SQL injection!)
        /// </summary>
        /// <param name="query">The SQL query to execute</param>
        /// <param name="parameters">An array of SqlParameters</param>
        /// <returns>A SqlDataReader containing the result set of the query</returns>
        public SqlDataReader ExecuteQueryString (string query, SqlParameter[] parameters=null)
        {
            // Create SQL statement
            SqlCommand stmt = new SqlCommand()
            {
                CommandText = query,
                Connection = this.con
            };

            // Bind the parameters
            if (parameters != null)
            {
                foreach (SqlParameter parm in parameters)
                {
                    stmt.Parameters.Add(parm);
                }
            }

            // Execute statement and return the results
            SqlDataReader results = stmt.ExecuteReader();
            stmt.Parameters.Clear();
            return results;
        }

        /// <summary>
        /// Convert a SqlDataReader to a Dictionary array
        /// </summary>
        /// <param name="reader">The SqlDataReader object</param>
        /// <returns>An array of dictionaries containing the results from the SqlDataReader</returns>
        public Dictionary<string,string>[] DataReaderToDict(SqlDataReader reader)
        {
            List<Dictionary<string, string>> results = new List<Dictionary<string, string>>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, string> result = new Dictionary<string, string>();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        result[reader.GetName(i)] = reader.GetValue(i).ToString().Trim();
                    }
                    results.Add(result);
                }
            }
            return results.ToArray();
        }

        [Serializable]
        class DatabaseConnectionException : System.Exception
        {
            public DatabaseConnectionException()
            : base() { }

            public DatabaseConnectionException(string message)
            : base(message) { }

            public DatabaseConnectionException(string format, params object[] args)
            : base(string.Format(format, args)) { }

            public DatabaseConnectionException(string message, System.Exception innerException)
            : base(message, innerException) { }

            public DatabaseConnectionException(string format, System.Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

            public DatabaseConnectionException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
        }

        public void Dispose()
        {
            con.Dispose();
        }
    }
}
